import React, {Component} from 'react';
import {connect} from "react-redux";
import {closeToModal, deleteToContact, getContacts, getInfoContact} from "../../store/action";
import './Contacts.css'
import Modal from "../../components/UI/Modal/Modal";
import InfoContact from "../../components/InfoContact/InfoContact";

class Contacts extends Component {
    componentDidMount() {
        this.props.getContacts()
    }

    render() {
        return (
            <div className="list-contacts">
                <Modal close={this.props.closeToModal} show={this.props.showModal}>
                   <InfoContact delete={this.props.deleteToContact} info={this.props.infoContact}/>
                </Modal>
                {this.props.contacts.map((contact, id) => {
                    return (
                        <div onClick={() => this.props.getInfoContact(contact.id)}  key={id} className="contact">
                            <img className="img-contact" src={contact.Photo} alt=""/>
                            <div className="contact-name">
                                <h3>{contact.name}</h3>
                            </div>
                        </div>
                    )
                })}
            </div>
        );
    }
}

const mapStateToProps = state =>({
    contacts: state.contacts,
    infoContact: state.infoContact,
    showModal: state.showModal

});


const mapDispachToProps = dispatch => ({
    getContacts: () => dispatch(getContacts()),
    getInfoContact: (id) => dispatch(getInfoContact(id)),
    closeToModal: () => dispatch(closeToModal()),
    deleteToContact: (id) => dispatch(deleteToContact(id))
});



export default connect(mapStateToProps, mapDispachToProps)(Contacts);