import React, {Component} from 'react';
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";
import {postToContact} from "../../store/action";
import connect from "react-redux/es/connect/connect";

class AddFormNewContact extends Component {
    state = {
      name: '',
      number: '',
      email: '',
      Photo: ''
    };

    valueChange = event => {
        const name = event.target.name;
        this.setState({[name]: event.target.value})
    };

    contactHandler = event => {
        event.preventDefault();
      const dataContact = {...this.state}  ;

      this.props.postToContact(dataContact, this.props.history)
    };

    backToContacts = () => {
        this.props.history.push('/')
    };

    render() {
        return (
            <Form>
                <h1 className="pt-5 pb-5">Add new contact</h1>
                <FormGroup row>
                    <Label for="exampleEmail" sm={2}>Name</Label>
                    <Col sm={10}>
                        <Input value={this.state.name} onChange={this.valueChange} type="text" name="name" />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label  sm={2}>Phone</Label>
                    <Col sm={10}>
                        <Input value={this.state.number} onChange={this.valueChange} type="number" name="number"  />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="exampleEmail" sm={2}>Email</Label>
                    <Col sm={10}>
                        <Input value={this.state.email} onChange={this.valueChange} type="email" name="email"  />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="exampleEmail" sm={2}>Photo</Label>
                    <Col sm={10}>
                        <Input value={this.state.Photo} onChange={this.valueChange} type="text" name="Photo" id="exampleEmail"/>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="exampleEmail" sm={2}>Photo preview</Label>
                    <Col sm={10}>
                        <img style={{width: '180px', height: '200px'}} src={this.state.Photo} alt=""/>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Col sm={10}>
                        <Button onClick={this.contactHandler}>Save</Button>
                        <Button onClick={this.backToContacts} className="ml-5">Back to contacts</Button>
                    </Col>
                </FormGroup>
            </Form>
        );
    }
}

const mapStateToProps = state =>({
    showModal: state.showModal

});


const mapDispachToProps = dispatch => ({
    postToContact: (dataContact, history) => dispatch(postToContact(dataContact, history))
});





export default connect(mapStateToProps, mapDispachToProps)(AddFormNewContact);