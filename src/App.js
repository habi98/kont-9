import React, { Component } from 'react';
import {Route} from "react-router-dom";

import './App.css';
import Layout from "./components/Layout/Layout";
import AddFormNewContact from "./containers/AddFormNewContact/AddFormNewContact";
import Contacts from "./containers/Contacts/Contacts";

class App extends Component {
  render() {
    return (
      <Layout>
        <Route path="/" exact component={Contacts} />
        <Route path="/add/contacts" component={AddFormNewContact}/>
      </Layout>
    );
  }
}

export default App;
