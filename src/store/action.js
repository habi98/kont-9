import axios from '../axios-contacts'
export const FETCH_CONTACTS_REQUEST = 'FETCH_CONTACTS_REQUEST ';
export const FETCH_CONTACTS_SUCCESS = 'FETCH_CONTACTS_SUCCESS ';

export const POST_CONTACT_REQUEST = 'POST_CONTACT_REQUEST';
export const POST_CONTACT_SUCCESS = 'POST_CONTACT_SUCCESS';

export const FETCH_INFO_CONTACT_SUCCESS = 'FETCH_INFO_CONTACT_SUCCESS';

export const DELETE_CONTACT_SUCCESS = 'DELETE_CONTACT_SUCCESS';


export const CLOSE_MODAL = 'CLOSE_MODAL';

export const fetchCntactsSuccess = (contacts) => ({type: FETCH_CONTACTS_SUCCESS, contacts});

export const postContactRequest = () => ({type: POST_CONTACT_REQUEST});

export const getInfoToContact = (info) => ({type: FETCH_INFO_CONTACT_SUCCESS, info});

export const deleteContact = () => ({type: DELETE_CONTACT_SUCCESS});

export const closeModal = () => ({type: CLOSE_MODAL});



export const getContacts = () => {
   return dispatch => {
       axios.get('Contacts.json').then(response => {
           const contacts = Object.keys(response.data).map(id => {
               return {...response.data[id], id}
           });
           dispatch(fetchCntactsSuccess(contacts))
       })
   }
};

export const postToContact = (dataCantact, history) => {
    return dispatch => {
        dispatch(postContactRequest());
        axios.post('Contacts.json', dataCantact).then(() => {
            history.push('/');
            dispatch(getContacts())
        })
    }
};

export const getInfoContact = (id) => {
    return dispatch => {
        axios.get('Contacts/'+ id +'.json').then(response => {
            dispatch(getInfoToContact({...response.data, id}));

        })
    }
};

export const deleteToContact = (id) => {
    return dispatch => {
        axios.delete('Contacts/'+ id +'.json').then(()=> {
            dispatch(deleteContact());
            dispatch(getContacts())
        })
    }
};

export const closeToModal = () => {
    return dispatch => {
       dispatch(closeModal())
    }
};
