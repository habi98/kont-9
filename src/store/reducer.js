import {CLOSE_MODAL, DELETE_CONTACT_SUCCESS, FETCH_CONTACTS_SUCCESS, FETCH_INFO_CONTACT_SUCCESS} from "./action";

const initialState = {
   contacts: [],
    infoContact: null,
    showModal: false
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_CONTACTS_SUCCESS:
            return {
                ...state,
                contacts: action.contacts
            };
        case FETCH_INFO_CONTACT_SUCCESS:
            return {
                ...state,
                infoContact: {...action.info},
                showModal: true
            };
        case DELETE_CONTACT_SUCCESS:
            return {
              ...state,
              showModal: false
            };
        case CLOSE_MODAL:
            return {
                ...state,
                showModal: false
            };
        default:
            return state
    }
};


export default reducer;