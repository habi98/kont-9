import React from 'react';
import {Button, Collapse, Nav, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";

const Header = () => {
    return (
        <Navbar color="light" light expand="md">
            <NavbarBrand href="/">Contacts</NavbarBrand>
            <NavbarToggler />
            <Collapse  navbar>
                <Nav className="ml-auto" navbar>
                    <NavItem>
                        <NavLink tag={RouterNavLink} to="/add/contacts"><Button>Add New contact</Button></NavLink>
                    </NavItem>
                </Nav>
            </Collapse>
        </Navbar>
    );
};

export default Header;