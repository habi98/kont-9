import React, {Fragment} from 'react';
import './InfoContact.css'

const InfoContact = (props) => {
    return (
        <div>
            {props.info &&
                <Fragment>
                    <img className='info-img' src={props.info.Photo} alt=""/>
                    <div className="info">
                        <h3>{props.info.name}</h3>
                        <p>Number: +{props.info.number}</p>
                        <p>Email: {props.info.email}</p>
                    </div>
                    <div className="buttons">
                        <button className="button">Edit</button>
                        <button onClick={() => props.delete(props.info.id)} className="button">Delite</button>
                    </div>
                </Fragment>
            }
        </div>
    );
};

export default InfoContact;