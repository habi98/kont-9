import React, {Fragment} from 'react';
import Header from "../Header/Header";
import {Container} from "reactstrap";

const Layout = ({children}) => {
    return (
        <div>
            <Fragment>
               <Header/>
              <Container>
                  {children}
              </Container>
            </Fragment>
        </div>
    );
};

export default Layout;